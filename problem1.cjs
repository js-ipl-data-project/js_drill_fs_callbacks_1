const fs = require("fs");

function createDirectory(directoryPath, callback) {
  fs.mkdir(directoryPath, (err) => {
    if (err) {
      console.log("Creation of folder not successful");
    } else {
      console.log("Creation of folder is successful");
      callback();
    }
  });
}

function createRandomJsonFiles() {
  for (let i = 0; i < 6; i++) {
    fs.writeFile(`random${i}.json`, "", (err) => {
      if (err) {
        console.log(`some thing went wrong`);
      } else {
        console.log(`Created random${i}.json`);
      }
    });
  }
}

function deleteFiles() {
  for (let i = 0; i < 6; i++) {
    fs.unlink(`random${i}.json`, (err) => {
      if (err) {
        console.log(`Not deleted random${i}.json`);
      } else {
        console.log(`Deleted random${i}.json`);
      }
    });
  }
}

module.exports = { createDirectory, createRandomJsonFiles, deleteFiles };
