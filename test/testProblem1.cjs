const {
    createDirectory,
    createRandomJsonFiles,
    deleteFiles,
  } = require("../problem1.cjs");
  
  const directoryPath = "folder";
  
  // Test the createDirectory function
  createDirectory(directoryPath, () => {
    console.log("Directory created successfully.");
    createRandomJsonFiles();
  
    deleteFiles();
  });
  