const fs = require('fs');


function readLipsumFile(callback){
    fs.readFile("public/lipsum.txt",(err,data)=>{
        if(err){
            console.log("file not found")
        }else{
            console.log("successfull readed lipsum.txt")
            //console.log(data.toString())
            callback(data.toString())

        } 
    })
}
function convertToUppercaseAndWite(data,callback){
    //console.log(data)
    let upperCase=data.toUpperCase();
   // console.log(upperCase)
    fs.writeFile("upperText.txt",upperCase,(err)=>{
        if(err){
            console.log("file not created")
        }else{
            console.log("converted uppercase and file created successfully")
            fs.appendFile("public/filenames.txt","upperText.txt,",(err)=>{
                if(err){
                    console.log("file not append")
                }else{
                    console.log("succeess")
                    callback()
                }
            })
        }
    })
}
function processNewFile(callback){
    fs.readFile("upperText.txt",(err,data)=>{
        if(err){
            console.log("not reading the file")
        }else{
            
             const lowercaseContent = data.toString().toLowerCase();
            // console.log(lowercaseContent)
            const sentences = lowercaseContent.split(". ");
           // console.log(sentences)
            fs.writeFile("splitedtext.txt",JSON.stringify(sentences),(err)=>{
                if(err){
                    console.log("spilted.txt getting error")
                }else{
                    console.log("process content return file")
                    fs.appendFile("public/filenames.txt","splitedtext.txt,",(err)=>{
                        if(err){
                            console.log("file not append")
                        }else{
                            console.log("succeess")
                            callback()
                        }
                    })
                }
            })
        }
        }
    )}
   
     function sortAndWrite(callback) {
        fs.readFile("splitedtext.txt",(err,data)=>{
            if(err){
                console.log("not reading the file")
            }else{
                console.log("sucess reading lowertext.txt")
                 let sortedData=JSON.parse(data.toString()).sort()
                // console.log(sortedData)
                fs.writeFile('sorttext.txt', JSON.stringify(sortedData), (err) => {
                    if (err) {
                        console.error('not create new file');
                        
                    } else {
                        console.log('Sorted content written to filenames.txt');
                        fs.appendFile("public/filenames.txt","sorttext.txt,",(err)=>{
                            if(err){
                                console.log("file not append")
                            }else{
                                console.log("succeess")
                                callback()
                            }
                        })
                
                    }
                });
      } })       
    }
    function deleteFiles(){
        fs.readFile("public/filenames.txt",(err,data)=>{
            if(err){
                console.log("error in filenames.txt")
            }else{
                console.log("success")
               // console.log(data.toString())
               
               data.toString().split(",").forEach((element)=>{
                 if(element!=""){
                      fs.unlink(element,(err)=>{
                    if(err){
                        console.log("not deleted")
                    }else{
                        console.log("successfully Deleted",element)
                    }
                })
                 }
               })
                 
            }
        })
    }

  module.exports = { readLipsumFile,convertToUppercaseAndWite, processNewFile, sortAndWrite, deleteFiles };
